import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author edalx.
 */
public class Test {
    public static void main(String[] args) {

        List<Integer> magia = new ArrayList();
        List<Integer> distancia = new ArrayList();


        //Test
        magia.add(2);
        magia.add(4);
        magia.add(5);
        magia.add(2);

        distancia.add(4);
        distancia.add(3);
        distancia.add(1);
        distancia.add(3);

        int resultado = aladinRefactor(magia, distancia);

        System.out.println(resultado);


    }

    public static int aladin(List<Integer> magia, List<Integer> distancia) {
        int aux = -1;
        for (int i = 0; i < distancia.size(); i++) {
            for (int j = 0; j < magia.size(); j++) {
                if (j > i) {
                    return j;
                }
            }
        }
        return aux;
    }


    public static int aladinRefactor(List<Integer> magia, List<Integer> distancia) {
        int m;
        int magiaDisponible = 0;
        int distRecorrer=distancia.get(0);

        for (int i = 0; i < distancia.size(); i++) {
            m = filtrarMagia(distRecorrer, magia);
            magiaDisponible += m; //Magia disponible
            if (magiaDisponible < 0) {
                return -1; //no pudo concluir el viaje
            } else {
                magiaDisponible-=distRecorrer;
                distRecorrer=magiaDisponible-distancia.get(i);
            }
        }

        return magiaDisponible;


    }


    public static int filtrarMagia(int distancia, List<Integer> magiaList) {
        List<Integer> distAux = magiaList.stream().filter(x -> x >= distancia).collect(Collectors.toList()); //Obtenemos todos la magia que sea mayor igual a la distancia
        int magia = -1;
        if (distAux.size() > 0) {
            Optional<Integer> magiaAux = distAux.stream().min(Integer::compareTo); //Obtenemos la distancia mínima del filtro
            if (magiaAux.isPresent()) {
                magia = magiaAux.get();
                magiaList.remove(magiaList.indexOf(magia));
            }
        }
        return magia;
    }

}


